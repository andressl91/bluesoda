PROJECT_WORKSPACE=${PROJECT_WORKSPACE:=$(readlink --canonicalize --no-newline `dirname ${0}`/../)}
PROJECT_DEPENDENCIES=$PROJECT_WORKSPACE/dependencies

cmake_common="-DCMAKE_INSTALL_MESSAGE=NEVER"

cd ${PROJECT_DEPENDENCIES}
if [ ! -d doctest ]; then
    echo Building doctest
    git clone https://github.com/onqtam/doctest
    pushd doctest
    cmake . -DCMAKE_INSTALL_PREFIX="${PROJECT_DEPENDENCIES}" ${cmake_common} -DCMAKE_CXX_STANDARD=17
    make install -j4
    popd
fi;
echo Done doctest

if [ ! -d criterion ]; then
    echo Building criterion
    git clone https://github.com/p-ranav/criterion
    pushd criterion 
    #mkdir build && cd build
    #cmake .. -DCMAKE_INSTALL_PREFIX="${PROJECT_DEPENDENCIES}" ${cmake_common} -DCMAKE_CXX_STANDARD=17
    #make install
    bash generate_single_include.bash
    cp -rf single_include/* ${PROJECT_DEPENDENCIES}/include
    popd
fi;


cd "${PROJECT_DEPENDENCIES}"
boost_ver=1_75_0
if [ ! -d boost_${boost_ver} ]; then
    echo Building boost_${boost_ver}
    if [ ! -f boost_${boost_ver}.tar.gz ]; then
        wget -O boost_${boost_ver}.tar.gz https://dl.bintray.com/boostorg/release/${boost_ver//_/.}/source/boost_${boost_ver}.tar.gz
    fi;
    tar -xf boost_${boost_ver}.tar.gz
    pushd boost_${boost_ver}
    ./bootstrap.sh --prefix="${PROJECT_DEPENDENCIES}"
    #qpy_root=${SHYFT_WORKSPACE}/miniconda/envs  # here we could tweak using virtual-env, conda or system.. match with cmake
    # have to help boost figure out right python versions bin,inc and libs.
    # first remove any python that was found with the bootstrap step above
    
    #mv -f project-config.jam x.jam
    #cat x.jam | sed -e 's/using python/#using python/g' >project-config.jam
    #echo "# injected by shyft build/build_dependencies.sh to map explicit python versions" >>project-config.jam
    #echo "using python : 3.7 : ${py_root}/shyft_37/bin/python : ${py_root}/shyft_37/include/python3.7m : ${py_root}/shyft_37/lib ;" >>project-config.jam
    
    boost_packages="--with-system --with-filesystem --with-date_time --with-python --with-serialization --with-chrono --with-thread --with-atomic --with-math python=3.8"
    ./b2 -j4 -d0 link=shared variant=release threading=multi ${boost_packages}
    ./b2 -j4 -d0 install link=shared variant=release threading=multi   ${boost_packages}
    popd
fi;
echo  Done boost_${boost_ver}

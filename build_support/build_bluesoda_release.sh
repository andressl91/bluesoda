PROJECT_WORKSPACE=${PROJECT_WORKSPACE:=$(readlink --canonicalize --no-newline `dirname ${0}`/../)}
cd ${PROJECT_WORKSPACE} && mkdir build || rm -rf build && mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug
make -j4
make install

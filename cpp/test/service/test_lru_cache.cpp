#include <map>
#include <string>
#include "doctest/doctest.h"
//#include "bluesoda/model/spam_model.h"
#include "bluesoda/service/lru_cache.h"


template <typename K,typename V> 
    struct lru_cache_map { 
    using type = lru_cache<K,V,std::map>; 
    }; 

TEST_SUITE("lru_cache_simple") {
    TEST_CASE("lru_cache_map"){
    lru_cache_map<std::string, std::string>::type string_string_cache(2);
    std::string key = "foo";
    std::string value = "bar";
    

    // Insert a key, value pair
    CHECK(string_string_cache.elements() == 0);
    string_string_cache.add(key, value);
    
    // Check key is in cache and value is what we stored
    CHECK(string_string_cache.contains(key));
    auto response = string_string_cache.get(key);
    CHECK(response == value);
    // Check entries has increased by +1
    CHECK(string_string_cache.elements() == 1);
    
    key = "egg";
    value = "bacon";
    string_string_cache.add(key, value);
    CHECK(string_string_cache.elements() == 2);
    
    // Cache capacity reached, no new elements should be added, only replace least used element
    key = "left";
    value = "right";
    string_string_cache.add(key, value);
    CHECK(string_string_cache.elements() == 2);
    // Least used element should now be first entry, lets verified it's gone
    CHECK(!string_string_cache.contains("foo"));
    
    // Trying to get a key not located in lru_cache, should raise runtime_error
    CHECK_THROWS_AS(string_string_cache.get("foo"), const std::runtime_error);
    
    
    // Remove second entry
    string_string_cache.remove("egg");
    // Trying to get a key not located in lru_cache, should raise runtime_error
    CHECK_THROWS_AS(string_string_cache.get("egg"), const std::runtime_error);
    // Check entries has decreased by -1
    CHECK(string_string_cache.elements() == 1);
    
      // Remove third entry
    string_string_cache.remove("left");
    // Trying to get a key not located in lru_cache, should raise runtime_error
    CHECK_THROWS_AS(string_string_cache.get("left"), const std::runtime_error);
    // Check entries has decreased by -1
    CHECK(string_string_cache.elements() == 0);
    }
}



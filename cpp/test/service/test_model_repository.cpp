#include <string>

#include "bluesoda/model/model.h"
#include "bluesoda/model/spam_model.h"
#include "bluesoda/service/repository.h"
#include "doctest/doctest.h"

TEST_SUITE("model_repositoryl"){
    TEST_CASE("foobar"){
    ModelRepository<SpamModel> spam_model_repo(2);
    
    SpamModel sm1(2, std::string{"spam"});
    // Make this work
    auto capacity = spam_model_repo.get_capacity();
    CHECK(capacity == 2);
    
    spam_model_repo.store_model(sm1);
    
    auto read_model = spam_model_repo.read_model(sm1.get_model_id());
    CHECK(read_model == sm1);
}
}

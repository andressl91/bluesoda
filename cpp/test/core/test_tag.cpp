#include "doctest/doctest.h"
#include "bluesoda/core/tag.h"

TEST_SUITE("core") {
    TEST_CASE("tag") {

        // Verify default constructor
        Tag tag;
        CHECK_EQ(tag.get_id(), 0);
        CHECK_EQ(tag.get_info(), "");

        Tag shirt(1, "blue");
        tag.set_tag("cheap");
        tag.set_tag("pocket");

        CHECK_EQ(shirt.get_id(), 1);
        CHECK_EQ(shirt.get_info(), "blue");
        CHECK(tag.has_tag("cheap") == true);
        CHECK(tag.has_tag("pocket") == true);
        CHECK(tag.has_tag("expensive") == false);

    }
}

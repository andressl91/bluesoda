#include <string>
#include <fstream>
#include <boost/archive/tmpdir.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include "doctest/doctest.h"
#include "bluesoda/core/item.h"


TEST_SUITE("core") {
    TEST_CASE("item") {
        Item my_item;
        CHECK_EQ(my_item.get_id(), 0);
        Item second_item(10);
        CHECK_EQ(second_item.get_id(), 10);

        // Check == and != Operatiors
        Item another_item(10);
        CHECK_NE(my_item, another_item);
        CHECK_EQ(second_item, another_item);
    }

    TEST_CASE("db_item") {

        DBItem db_item(21);
        CHECK_EQ(db_item.get_id(), 21);

        // Create temporary directory and file for testing serialization
        std::string tmp_file(boost::archive::tmpdir());
        tmp_file += "/binary.db";

        // Close archive after successful stream
        {
            std::ofstream ofs(tmp_file.c_str());
            boost::archive::binary_oarchive oa(ofs);
            oa << db_item;
        }

        // Restore the schedule from the archive
        // Close archive after successful stream
        DBItem restored_db_item;
        {
            std::ifstream ifs(tmp_file.c_str());
            boost::archive::binary_iarchive ia(ifs);
            ia >> restored_db_item;

        }

        CHECK_EQ(restored_db_item.get_id(), db_item.get_id());

    }

}

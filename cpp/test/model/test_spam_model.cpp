#include <boost/archive/tmpdir.hpp>
#include "doctest/doctest.h"
#include "bluesoda/model/spam_model.h"


TEST_SUITE("spam_model") {
    TEST_CASE("Spam"){
        SpamModel spam_model(10, "bacon");
        CHECK(spam_model.get_model_id() == 10);
        CHECK(spam_model.get_name() == "bacon");
    }
    
    TEST_CASE("Serialization_text_oarchive") {
        SpamModel spam_model(10, "bacon");
        std::string filename(boost::archive::tmpdir());
        filename += "/text_demofile.txt";
        
        save_spam_text_iarchive(spam_model, filename.c_str());
        CHECK(spam_model.get_model_id() == 10);
        
        SpamModel load_model;
        load_spam_text_oarchive(load_model, filename.c_str());
        
        CHECK(load_model.get_model_id() == 10);
        CHECK(load_model._spam._fat_percent == 10);
        CHECK(load_model.get_name() == "bacon");
        CHECK(load_model._int_container[0] == 10);
        
        }
        
        TEST_CASE("Serialization_binary_archive") {
        SpamModel spam_model(20, "fat_bacon");
        std::string filename(boost::archive::tmpdir());
        filename += "/binary_demofile.txt";
        
        save_spam_binary_iarchive(spam_model, filename.c_str());
        
        CHECK(spam_model.get_model_id() == 20);
        
        SpamModel load_binary_model;
        load_spam_binary_oarchive(load_binary_model, filename.c_str());
        
        CHECK(load_binary_model.get_model_id() == 20);
        CHECK(load_binary_model._spam._fat_percent == 20);
        CHECK(load_binary_model.get_name() == "fat_bacon");
        CHECK(load_binary_model._int_container[0] == 20);
        
        }
}



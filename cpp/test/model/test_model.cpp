#include "bluesoda/model/model.h"
#include "doctest/doctest.h"


TEST_SUITE("basic_model"){
    TEST_CASE("ModelBase") {
    ModelBase mb_1(10), mb_2(20);
    
    CHECK(mb_1.get_model_id() == 10);
    SUBCASE("equality") {
        ModelBase mb_3(20);
        CHECK(mb_1 != mb_2);
        CHECK(mb_2 == mb_3);
        }
    }
}



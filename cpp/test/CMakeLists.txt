set(test_source with_main.cpp
        model/test_model.cpp service/test_model_repository.cpp model/test_spam_model.cpp service/test_lru_cache.cpp
    net/test_message.cpp net/test_thread_safe_queue.cpp core/test_item.cpp core/test_tag.cpp) #net/test_client_server_io.cpp

set(target "test_bluesoda")
add_executable(${target} ${test_source})
# Mind the order of include_dir, if make install first, test will first find headers in PROJECT_D/include
# Reason for PROJECT_D/include is for doctest and other 3.party headers
target_include_directories(${target} PRIVATE ${CMAKE_SOURCE_DIR}/cpp ${PROJECT_DEPENDENCIES}/include)
target_link_libraries(${target} -lboost_serialization -pthread)
set_target_properties(${target} PROPERTIES CXX_STANDARD 17)
# Nice if tests need relative link path to project libraries
#set_target_properties(${target} PROPERTIES INSTALL RPATH "SOMEPATH"
install(TARGETS ${target} CONFIGURATIONS Debug DESTINATION ${CMAKE_SOURCE_DIR}/bin/debug)
install(TARGETS ${target} CONFIGURATIONS Release DESTINATION ${CMAKE_SOURCE_DIR}/bin/release)

set(all_tests "core" "basic_model" "spam_model" "lru_cache_simple" "model_repository"
        "net_message" "net_queue" ) #"net_client_server"
foreach(project_t ${all_tests})
        add_test(${project_t} ${target} -nv --test-suite=${project_t})
    endforeach(project_t)

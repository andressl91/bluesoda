
#include "doctest/doctest.h"
#include "bluesoda/service/thread_safe_queue.h"

TEST_SUITE("net_queue") {
    TEST_CASE("queue manipulation") {
        
        class Connection {
        public:
            int remote;
            Connection(int r) : remote{r} {}           
        };
        
         Connection _conn1(2);
         Connection _conn2(4);
         Connection _conn3(6);
        
        bluesoda::net::mqueue<Connection> conn_queue;
        
        CHECK(conn_queue.empty() == true);
        
        conn_queue.emplace_front(_conn1);
        conn_queue.emplace_front(_conn2);
        conn_queue.emplace_back(_conn3);
        // We should have 3 items now
        CHECK(conn_queue.count() == 3);
        
        // Queue should be, _conn2 -> conn1 -> _conn3;
        auto queue_front = conn_queue.front();
        auto queue_back = conn_queue.back();
        CHECK(queue_front.remote == _conn2.remote);
        CHECK(queue_back.remote == _conn3.remote);
        CHECK(queue_back.remote == _conn3.remote);
        
        // Let's get rid of the queue
        auto item1 = conn_queue.pop_front();
        CHECK(item1.remote == _conn2.remote);
        CHECK(conn_queue.count() == 2);
        
        auto item2 = conn_queue.pop_front();
        CHECK(item2.remote == _conn1.remote);
        CHECK(conn_queue.count() == 1);
        
        auto item3 = conn_queue.pop_back();
        CHECK(item3.remote == _conn3.remote);
        CHECK(conn_queue.count() == 0);
        
        CHECK(conn_queue.empty() == true);
    }
}



#include "doctest/doctest.h"
#include "bluesoda/net/message.h"

TEST_SUITE("net_message") {
    TEST_CASE("Insert/Extract") {
        
        // Denfine some userdefined messagetypes.
        enum class BasicMsgTypes : uint16_t {
            SendModel,
            GetModel,
            SearchModel
        };
        // Define a simple model
        struct Model {
            int mdl_id = 0;
        };
        
        // Add this simple model and an integer to message
        Model ml1;
        int a = 5;
        ml1.mdl_id = 2;
        
        
        bluesoda::net::message<BasicMsgTypes> mesg;
        mesg.header.id = BasicMsgTypes::SendModel;
        mesg << ml1 << a; 
        
        // Check message header and bodysize is set
        CHECK(mesg.header.id == BasicMsgTypes::SendModel);
        CHECK(mesg.size() == mesg.header.size);
        
        Model ml2;
        int b;
        mesg >> b >> ml2;
        CHECK(ml2.mdl_id == 2);
        CHECK(b == 5);
        
    }
    
}

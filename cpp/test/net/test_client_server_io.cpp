#include <string>

#include <doctest/doctest.h>

#include <bluesoda/net/client.h>
#include <bluesoda/net/server.h>
#include <bluesoda/net/message.h>

#include <chrono>
#include <thread>



using namespace bluesoda::net;
enum class MessageProtocol : uint32_t {
    Accept,
    Disconnet,
    Pulse
};

struct Pulse {
    int x = 4;
    Pulse(int i=0)
            : x{i} {}
};

class BlueClient : public client_interface<MessageProtocol> {
public:
    using m_connection = std::unique_ptr<connection<MessageProtocol>>;

    void send_pulse_message(int input=0) {

        message<MessageProtocol> msg;
        msg.header.id = MessageProtocol::Pulse;
        Pulse pulse{input};
        msg << pulse;
        std::cout << "Send pulse message\n";
        send(msg);
    }


    void on_message(m_connection server, message<MessageProtocol> &msg) override {
        std::cout << "# Client: In on_message handler\n";
        switch (msg.header.id) {
            case MessageProtocol::Pulse: {
                std::cout << "# Client: Recieved Pulse Package in on_message\n";
                Pulse recieved_pkg(2);
                msg >> recieved_pkg;
                CHECK(recieved_pkg.x == 5);
                std::cout << recieved_pkg.x << std::endl;
            }
        }
    }
};

class BlueServer : public server_interface<MessageProtocol> {
    using m_connection = std::shared_ptr<connection<MessageProtocol>>;
public:
    BlueServer(uint16_t port) : server_interface<MessageProtocol>(port) {}

protected:

    virtual bool on_connect  (m_connection client) {
        std::cout << "Here we can do something with -> " << client->remote_endpoint() << std::endl;
        return true;
    }

    virtual void on_disconnect(m_connection client) {

    }
    virtual void on_message(m_connection client, message<MessageProtocol>& msg) {
        switch (msg.header.id) {

            case MessageProtocol::Accept : {
                std::cout << "# Server: Client " << client->remote_endpoint() << " with id: " << client->get_id()
                          << " sendt a Accept message protocol\n";
            };
                break;
            case MessageProtocol::Pulse : {
                std::cout << "# Server: Client " << client->remote_endpoint() << " with id: " << client->get_id()
                          << " sendt a Pulse message protocol\n";
                std::cout << "$ Server: Sending msg back to client, adding x++ to object\n";
                Pulse p;
                msg >> p;
                p.x += 1;
                msg << p;
                client->send(msg);
            };
                break;

        }
    }

};


TEST_SUITE("net_client_server") {
    TEST_CASE("io") {

        auto _worker = std::thread([]() {
            BlueServer server(40405);
            server.start_async();
            std::cout << "Sleep for 5 seconds\n";
        std::chrono::seconds dura( 3);
        std::this_thread::sleep_for( dura);
        server.process_messages(-1);
        std::this_thread::sleep_for( dura);
        server.process_messages(-1);
        std::this_thread::sleep_for( dura);
        server.process_messages(-1);
        server.stop_async();
        } );

    auto _client = std::thread([]() {
        BlueClient client;
        std::string endpoint = "127.0.0.1";
        std::chrono::seconds dura(1);
        std::this_thread::sleep_for(dura);
        client.connect(endpoint, 40405);

        client.send_pulse_message(4);

        bool stay = true;
        while(stay) {
            if (client.is_connected()) {
                if (!client.incoming_messages().empty()) {
                    auto mesg = client.incoming_messages().pop_front().msg;

                    switch (mesg.header.id) {
                        case MessageProtocol::Pulse: {
                            Pulse recieved_pkg(2);
                            mesg >> recieved_pkg;
                            // Server configuret to add 1 on Pulse Header
                            CHECK(recieved_pkg.x == 5);
                            std::cout << "# Client: Recieved Pulse Package\n";
                            std::cout << recieved_pkg.x << std::endl;
                            stay = false;
                        }
                            break;
                    }
                }
            }
            else {
                stay = false;
            }
        }
        message<MessageProtocol> msg;
        Pulse p(4);
        msg.header.id = MessageProtocol::Pulse;
        msg << p;
        std::cout << "Trying to send with response\n";
        client.send(msg,true, 5000);

        client.disconnect();
    });

    if (_worker.joinable()) {
        _worker.join();
    }

    if (_client.joinable()) {
        _client.join();
    }


    }
}

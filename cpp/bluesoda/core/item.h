#pragma once

#include <cstdint>
#include <boost/archive/binary_iarchive.hpp>

struct Item {
public:
    explicit Item(int64_t _id=0) : id{_id} {};
    int64_t get_id() const {return id;}

    bool operator==(const Item& other) const {
        return id == other.get_id();
    }
    bool operator!=(const Item& other) const {
        return !this->operator==(other);
    }

protected:
    int64_t id;
};

struct DBItem : public Item {

public:
    explicit DBItem(int64_t _id=0) : Item(_id) {};

private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
        // serialization of base class
        //ar & boost::serialization::base_object<Item>(*this);
        ar & id;
    }
};
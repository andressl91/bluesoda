#pragma once

#include <cstdint>
#include <string>
#include <unordered_set>
#include <utility>

struct Tag {
public:
     explicit Tag(int64_t id=0, std::string info="") : id{id}, info{std::move(info)}
     {
        tags.insert("");
     };

    int64_t get_id() const {return id;}
    std::string get_info() const {return info;}

    void set_tag(const std::string& _tag)
    {
        tags.insert(_tag);
    }

    bool has_tag(const std::string& _tag)
    {
        if (tags.find(_tag) == tags.end())
            return false;
        return true;
    }


protected:
    int64_t id;
    std::string info;
    std::unordered_set<std::string> tags;
};
#pragma once
#include <boost/asio.hpp>

#include "bluesoda/service/thread_safe_queue.h"
#include "message.h"

namespace bluesoda {
    namespace net {
        
        using namespace boost;
        
        template<typename T>
        // Allow creation of shared_ptr for this, rather than raw pointer within object.
        class connection : public std::enable_shared_from_this<connection<T>> {
        public: 
            
            enum class owner {
                server,
                client
            };
            
            connection(owner owner, asio::io_context& io_context,
                       asio::ip::tcp::socket socket, mqueue<owned_message<T>>& owned_m) 
            : _owner(owner), _io_context(io_context), _socket(std::move(socket)), _messages_in(owned_m) {
                
            }
            
            virtual ~connection() {}
        
        /**
         * @brief Server specific, connect to client.
         * @details On connection give connection an id, and initiate async read_header method.
         */
        void connect_to_client(uint32_t i=0) {
            if (_owner == owner::server) {
                if (_socket.is_open()) {
                    id = i;
                    read_header();
                }
            }
        }
        
        
        void connect_to_server(const asio::ip::tcp::resolver::results_type& endpoints) {
            if(_owner == owner::client) {
                asio::async_connect(_socket, endpoints, 
                    [this](std::error_code error, asio::ip::tcp::endpoint endpoint) {
                    
                        if (!error) {
                            read_header();
                        }
                        else {
                            std::cout << "# Client: failed to connect with: " << error.message() << "\n";
                        }
                    }
                        
            );}
        }
        /**
         * Return enpoint hostname/ip of the the endpoint connected to this connection.
         * @return
         */
        auto remote_endpoint(){
            return _socket.remote_endpoint();
        }
        
        /**
         * @brief Close the socket related to the connection instance.
         */
        
        bool disconnect() {
            if (is_connected()) {
                asio::post(_io_context, [this]() {_socket.close();});
                return true;
            }
            return false;
        }
            
        // TODO: Not accurate enough
        bool is_connected() const {
            if (_socket.is_open()) {
                return true;
            }
            return false;
            
        }
            
        
        /**
         * @brief Send a message to a endpoint
         * 
         * @details 
         * 
         * @details The message is posted to the messages out queue, for prosessing.
         * If the messages out queue is not empty, we can assume the async thread is in the prosess
         * of writing/sending another message to an endpoint, and we simply add the message to
         * the messages out queue. If the queue is empty, meaning no writing/sending is occuring, we 
         * initiate the async write_header function
         * @see write_header
         * 
         */
        void send(const message<T>& mesg) {
            asio::post(_io_context, 
                [this, mesg]() {
                    bool _is_writing_message = !_messages_out.empty();
                    _messages_out.emplace_back(mesg);
                    
                    if (!_is_writing_message) {
                        write_header();
                    }
                }
            );
        }
        
        uint32_t get_id() const{
            return id;
        }
        
        private:
        //protected:
            /**
             * @brief Read the message header from endpoint.
             * 
             * @details A connection does a async read when a endpoint is trying to connect. An asio buffer i set up, using a connection owned message, to store an incomming endpoint message. The expected buffer size, is is constructed based on the message_header definition.
             * @see message.h
             * If the connection fails on the serverside, the server implementation will catch this and remove the connection.
             */
            void read_header() {
                asio::async_read(_socket, asio::buffer(&_temp_message_in.header, sizeof(message_header<T>)),
                    [this](std::error_code error, std::size_t length) {
                    
                    if (!error) {
                        if (_temp_message_in.header.size > 0) {
                            _temp_message_in.body.resize(_temp_message_in.header.size);
                            read_body();
                        }
                        
                        else {
                            // We don't have a body for all incoming messages..
                            add_to_incoming_message_queue();
                        }
                    }
                    else {
                        std::cout << "# Server: Connection id = " << id << " read header failed "<< error.message() << std::endl;
                        _socket.close();
                    }
                    }
                );
            }
            
            /**
             * @brief Read the message body from endpoint.
             */
            void read_body() {
                // TODO: Possible memleak at sync_read buffer
                // body.data is the raw array of vector of size n, not including message header
                // We try to send .body.size() to the buffer which is sizeof(message_header<T>) + body.size(), we should only add the message body without header.;
                asio::async_read(_socket, asio::buffer(_temp_message_in.body.data(), _temp_message_in.body.size()), 
                    [this](std::error_code error, std::size_t length) {
                        
                        if(!error) {
                            add_to_incoming_message_queue();
                         }
                        else {
                            std::cout << "# Connection id = " << id << " read message failed "<< error.message() << std::endl;
                            _socket.close();
                         }
                         
                     }
                );
                
            }
            
            /**
             * @brief Construct message header from the outgoing message queue. 
             * @details If messages_out queue has elements, get the first element and 
             * prosess its header. If the element has a body, call the asio write_body
             * @details If the bodysize of the message is 0, simply remove message from queue as we don`t
             * want to send a message with no body.
             * @see write_body
             * @see message.h 
             */
            void write_header() {
                asio::async_write(_socket, asio::buffer(&_messages_out.front().header, sizeof(message_header<T>)), 
                    [this](std::error_code error, std::size_t length) {
                        if (!error) {
                            
                            if (_messages_out.front().body.size() > 0) {
                                write_body();
                            }
                            else {
                                _messages_out.pop_front();
                                
                                if (!_messages_out.empty()) {
                                    write_header();
                                }
                            }
                        }
                        else {
                            std::cout << "# Connection id = " << id << "write header failed "<< error.message() << std::endl;
                            _socket.close();
                        }
                      }
                    );
            }

            /**
             * @brief Construct message body.
             */
            void write_body() {
      
                asio::async_write(_socket, asio::buffer(_messages_out.front().body.data(), 
                                                        _messages_out.front().body.size()), 
                    [this](std::error_code error, std::size_t length) {
                        if (!error) {
                            _messages_out.pop_front();
                            
                            if (!_messages_out.empty()) {
                                write_header();
                            }
                        }
                        else {
                                std::cout << "# Connection id = " << id << "write body failed "<< error.message() << std::endl;
                            _socket.close();
                        }
            });
            }
            
            /**
             * @brief Add message from endpoint to incoming message queue.
             * @details If the connection is owned by a server, we add the message to incoming message queue.
             * @details If the connection is owned by a client, it only has (for now), one connection,
             * and we don`t add the connection.
             * @details function serves as the final endpoint for reading messages, hence we call
             * the asio read_header for parsing new incoming requests.
             */
            void add_to_incoming_message_queue() {
                if (_owner == owner::server) {
                    // Since a server ca
                    _messages_in.emplace_back({this->shared_from_this(), _temp_message_in});
                }
                else {
                    //owned_message<T> om(this->shared_from_this(), _temp_message_in);
                    //_messages_in.emplace_back(om);
                    _messages_in.emplace_back({nullptr, _temp_message_in});
                }
                read_header();
            }
            
            
        
        protected:
        // Who the connection is owned by.
        owner _owner = owner::server;
        
           // Single context, shared by all asio instances
        asio::io_context& _io_context;
            
        // Socket connected to endpoint
        asio::ip::tcp::socket _socket;
        
        // Threadsafe queue, holding all messages to be sent to endpoint
        // Connection instance responsibility
        mqueue<message<T>> _messages_out;
        
        // Threadsafe queue, holding all messages revieved from endpoint.
        // Note reference, provided by connection owner.
        mqueue<owned_message<T>>& _messages_in; 
        message<T> _temp_message_in;

        // Identification of who owns the connection.
        uint32_t id = 0;
        
        };
        
    }
    
}

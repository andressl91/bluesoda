#pragma once

#include <cstdint>
#include <cstring>
#include <vector>
#include <memory>
#include <iostream>


namespace bluesoda {
    
    namespace net {
        
        /**
         * @brief Fixed size message header send first in netorking.
         * 
         */
        
        template <typename T>
        struct message_header {
            T id{};
            uint32_t size = 0;
        };
        
        /**
         * @brief Datatype for sending messages over network. 
         * @details A message consists of a header and a body. 
         * @details The header is fixed size, and sent as the first packet over network. It informs the remote site of how mutch buffer must  be initiated to recieve the message body.
         */
        template <typename T>
        struct message {
          
            /**
             * @brief Return total size of message.
             * @return size of message body.
             */
            size_t size() const {
                return body.size();
            }
            
            friend std::ostream& operator << (std::ostream& os, const message<T>& msg) {
                os << "ID: " << int(msg.header.id) << "Size: " << msg.header.size;
            
                return os;
            }
            
            /**
             * @brief Add data to the mssage buffer.
             * @param msg the message holding the buffer.
             * @param data the data we want to add to the message buffer
             */
            template <typename DataType>
            friend message<T>& operator << (message<T>& msg, const DataType& data) {
                
                // Check at compiletime if DataType can be easily added to buffer
                static_assert(std::is_standard_layout<DataType>::value, "DataType is to complex for message buffer");
                
                // Cache the current size of the message body
                size_t body_size = msg.body.size();
                
                // Resize the message body for incoming data
                msg.body.resize(msg.body.size() + sizeof(DataType));
                
                //TODO: Alternative to memcpy
                // Copy incoming data to the message buffer
                std::memcpy(msg.body.data() + body_size, &data, sizeof(DataType));
                
                // Now recalculate the message size
                msg.header.size = msg.size();
                
                return msg;
                
            }
            
             template <typename DataType>
            friend message<T>& operator >>(message<T>& msg,  DataType& data) {
                
                // Check at compiletime if DataType can be easily added to buffer
                static_assert(std::is_standard_layout<DataType>::value, "DataType is to complex for message buffer");
                
                // Cache the location to add DataType
                size_t body_size = msg.body.size() - sizeof(DataType);
                
            
                
                //TODO: Alternative to memcpy
                // Copy message body to data
                std::memcpy(&data, msg.body.data() + body_size, sizeof(DataType));
                
                // Resize body without penalty
                msg.body.resize(body_size);
                
                // Now recalculate the message body size and add to header
                msg.header.size = msg.size();
                
                return msg;
                
            }
            // TODO: Resize vector based on protocol on higher level,
            // to avoid resizing each time when adding data to body.
            message_header<T> header{}; /**< Message header */
            std::vector<uint8_t> body; 
            
        };

        template<typename T>
        class  connection;
        
        /**
         * @brief Link a message to a connection
         * 
         */
        template <typename T> 
        struct owned_message {
            
            owned_message(std::shared_ptr<connection<T>> rm_connection, message<T> message) 
            : remote{rm_connection}, msg{message} {}
            
            // Connection object for transfer of message to remote side.
            std::shared_ptr<connection<T>> remote = nullptr;
            message<T> msg;
            

            
            friend std::ostream& operator <<(std::ostream& os, const owned_message<T>& msg) {
                os << msg.msg;
                return os;
            }
        };
        
    }
}
    

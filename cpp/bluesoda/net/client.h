#pragma once

#include <memory>
#include <string>
#include <thread>
#include <exception>
#include <chrono>

#include <boost/asio.hpp>

#include "message.h"
#include "connection.h"
#include "bluesoda/service/thread_safe_queue.h"

namespace bluesoda {
    namespace net {
        using namespace boost;
        
        template <typename T>
        class client_interface {
        public:
            
            client_interface() : _socket(_context) {}
            virtual ~client_interface() {
                disconnect();
            }
            
            /**
             * @brief Connect to enpoint, defined by a host and port
             */
            bool connect(const std::string& host, const uint16_t port) {
                try {
                    
                    
                    // Resolve provided host/port combination
                    asio::ip::tcp::resolver resolver(_context);
                    asio::ip::tcp::resolver::results_type _endpoints = resolver.resolve(host, std::to_string(port));
                    
                    // Create connection to endpoint
                    _connection = std::make_unique<connection<T>>(
                        connection<T>::owner::client, 
                        _context, 
                        asio::ip::tcp::socket(_context), 
                        _messages_in);
                    
                    // Connect to endpoint using the connection object.
                    _connection->connect_to_server(_endpoints);
                    
                    _context_thread = std::thread([this]() { _context.run(); });
                }
                
                catch (std::exception& e) {
                    std::cerr << "Client connect raised exception: " << e.what() << "\n";
                    return false;
                }
                return true;
            }
            /**
             * @brief Disconnect from endpoint.
             */
            void disconnect() {
                if(is_connected()) {
                    _connection->disconnect();
                }
                
                // Stop the asio _context and its working thread
                _context.stop();
                if (_context_thread.joinable()) {
                    _context_thread.join();
                }   
                
                //_connection.release();
                //TODO: Destroy connection
                //~connection(); ?
                
                
            }
            /**
             * @brief Check if client is connected to endpoint. 
             */
            bool is_connected() {
                if (_connection) 
                    return _connection->is_connected();
                return false;
            }
            
            mqueue<owned_message<T>>& incoming_messages() {
                return _messages_in;
            }
            
            void send(const message<T>& mesg, bool response=false, int wait_timeout=1000) {
                _connection->send(mesg);
                if (response) {
                    auto time_now = std::chrono::system_clock::now();
                    auto wait_max = time_now + std::chrono::milliseconds(wait_timeout);

                    //while (tme_now < wait_max) {}
                    bool wait = true;
                    //# TODO: Implement timeout
                    while (wait) {
//                    std::cout << "Diff count is : " << std::ctime(time_now.time_since_epoch()) << std::endl;
                        if (!incoming_messages().empty()) {
                            auto msg = incoming_messages().pop_front().msg;
                            on_message(std::move(_connection), msg);
                            wait = false;
                        } else {
                            time_now = std::chrono::system_clock::now();
                        }
                    }
                }
            }
        protected:
            /**
             * @brief Callback used in send_and_wait memberfunction, to act on recieved message.
             *
             * @param client
             * @param mesg
             * @see send_and_wait
             */
            virtual void on_message(std::unique_ptr<connection<T>> server, message<T>& mesg) {

            }

            asio::io_context _context;
            
            // Socket connected to endpoint
            asio::ip::tcp::socket _socket;

            
            // Worker thread for endpoint interaction
            std::thread _worker;
            std::thread _context_thread;
            

            // Single connection instance, to handle I/O messages.
            std::unique_ptr<connection<T>> _connection = nullptr;
            
        private:
            // Threadsafe queue, for holding incoming messages from endpoint.
            mqueue<owned_message<T>> _messages_in;
        };
        
    }
}

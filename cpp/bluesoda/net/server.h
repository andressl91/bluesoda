#pragma once


#include <iostream>
#include <memory>
#include <thread>
#include <exception>
#include <deque>

#include <boost/asio.hpp>

#include "message.h"
#include "connection.h"
#include "bluesoda/service/thread_safe_queue.h"


namespace bluesoda {
    namespace net {
            
        template<typename T>
        class server_interface {
        
        public:
            server_interface(uint16_t port) 
            : _acceptor(_io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)) {}
            
            virtual ~server_interface() {
                stop_async();
                
            }
            
            bool start_async() {
            try {
                listen_for_connections();
                // Initiate the io_context processing loop with work, so it doens't exit from the start.
                _worker = std::thread([this]() {_io_context.run(); });
            }
            
            catch (std::exception& e) {
                std::cerr << "# Server exception :  " << e.what() << "\n";
                return false;
            }
            std::cout << "# Server: Started! \n"; 
            return true;
            }
            
            /**
             * @brief Stop listening for connections, and drop active connections.
             */
            void stop_async() {
                
                // Stop the io_context processing loop.
                _io_context.stop();
                
                // Join the io_context worker thread
                if (_worker.joinable()) {
                    _worker.join();
                }
                
                std::cout << "# Server: Stopped! \n";
            }
            
                
            
            
            
            void listen_for_connections() {
                _acceptor.async_accept(
                    [this](std::error_code err, boost::asio::ip::tcp::socket socket) {
                        
                        if (!err) {
                            std::cout << "# Server: Client " << socket.remote_endpoint() << " connected\n";
                            
                            // Incoming connection "owned" by client.
                            auto new_client = std::make_shared<connection<T>>(connection<T>::owner::server,
                                                _io_context, std::move(socket), _incoming_messages);
                            
                            // Allow to act on client (respond, deny etc)
                            if (on_connect(new_client)) {
                                
                                // TODO: Emplace back
                                // Some user defined logic, allows client into active connections.
                                _active_connections.push_back(std::move(new_client));

                                // Siiue task to connections asio conxtext to wait for client transmission.;
                                _active_connections.back()->connect_to_client(_id_counter++);
                                
                                std::cout << "# Server: " << _active_connections.back()->get_id() << " approved\n"; 
                                
                            }
                            else {
                                
                                std::cout << "# Server: Denied client " << socket.remote_endpoint() << "\n";
                            }
                            
                        }
                        else {
                            std::cout << "# Server: Failed accepting connection -> " << err.message() << std::endl;
                        }
                        
                        // When processing of connection is done, head back and listen for more.
                        listen_for_connections();
                        
                    }
                
                    );
            }
            
            /**
             * @brief Send message to connected client
             */
            void send_message(std::shared_ptr<connection<T>> client, const message<T>& mesg) {
                
                // Validate shared_ptr and if client is still connected.
                if (client && client->is_connected) {
                        client->send(mesg);
                }
                else {
                    on_disconnect(client);
                    _active_connections.erase(
                        std::remove(_active_connections.begin(), _active_connections.end(), client), _active_connections.end());
                }
                
            }
            
            /**
             * @brief Send message to all active connected clients
             */
            void send_message_all(const message<T>& mesg, std::shared_ptr<connection<T>> ignore_client = nullptr) {
                
                // To avoid changing _active_connections iterator in for-loop
                // notify if some invalid client exists, and clean up after loop.
                bool some_invalid_client = false;
                    
                for (auto& client : _active_connections) {
                    
                    // Validate shared_ptr and if client is still connected
                    if (client && client->is_connected) {
                        // Check client isn't the ignored client.
                        if (client != ignore_client) {
                            client->send(mesg);
                        }
                        
                    }
                    
                    // Failed to contact client, assume the worst and bye bye.
                    else {
                        on_disconnect(client);
                        // Reset connection shared_ptr in active_connections 
                        client.reset();
                        some_invalid_client = true;
                    }
                }
                
                if (some_invalid_client) {
                    // Search for nullptrs after client.reset and remove from active_connections.
                    _active_connections.erase(
                        std::remove(_active_connections.begin(), _active_connections.end(), nullptr), _active_connections.end());
                }
                
            }
        
        /**
         * @brief Add/reduce maximum incoming messages from clients
         * size_t unsinged int, by setting -1, it is set to max messages.
         */
        // RENAME TO UPDATE OR SOMETHING
        void process_messages(size_t max_mesg = -1) {
            size_t message_count = 0;
            
            // While we have more messages than the maximum messages
            while (message_count < max_mesg && !_incoming_messages.empty()) {

                // Get the first message in deck
                auto mesg = _incoming_messages.pop_front();

                // Do the honors and call the message callback.
                on_message(mesg.remote, mesg.msg);
                
                message_count++;
            }
        }
            
        protected: 
            /**
             * @brief Callback function to be used when a client connects
             * @return boolean result if connection was accecpted or denied.
             */
            virtual bool on_connect(std::shared_ptr<connection<T>> client) {
                return false;
            }
            
            /**
             * @brief Callback function if client has lost connection or disconnected.
             */
            virtual void on_disconnect(std::shared_ptr<connection<T>> client) {
            }
            
            /**
             * @brief Callback function for handling incoming message from client
             * 
             */
            virtual void on_message(std::shared_ptr<connection<T>> client, message<T>& mesg) {
            }
            
            // Deck of connected clients 
            std::deque<std::shared_ptr<connection<T>>> _active_connections;
            
            // Queue for holding incoming (owned)-messages from clients.
            mqueue<owned_message<T>> _incoming_messages;
            
            boost::asio::io_context _io_context;
            std::thread _worker;
            
            
            // For capuring incoming connections
            boost::asio::ip::tcp::acceptor _acceptor;
            
            // Identiy connected clients by an ID
            uint32_t _id_counter = 0;
        };   
        
    }
}

#pragma once

#include <string>
#include <vector>
#include <boost/archive/text_iarchive.hpp>


struct ModelBase {

public: 
    int _model_id;
    constexpr ModelBase() : _model_id{0} {};
    constexpr ModelBase(int model_id) : _model_id{model_id} {};
    int get_model_id() const {return _model_id; };
    
    bool operator==(const ModelBase& other) const {
        return _model_id == other.get_model_id();
    }
    bool operator!=(const ModelBase& other) const {
        return !this->operator==(other);
    }
private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & _model_id;
    }
    
};



#pragma once

#include <vector>
#include <string>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/base_object.hpp>

#include <fstream>
#include <iostream>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>



#include "bluesoda/model/model.h"



struct Spam {
    int _fat_percent;
    
private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & _fat_percent;
    }
};

struct SpamModel : public ModelBase {
    
public:
    std::string _name;
    std::vector<int> _int_container;
    Spam _spam;
    // Vector not yet constexpr support
    SpamModel() = default;
    SpamModel(int model_id, std::string name) : 
    ModelBase{model_id}, _name{name}, _spam{model_id} {
        _int_container.emplace_back(model_id);
    }
    std::string get_name() { return _name;}
    

    
private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
        // serialization of base class
        ar & boost::serialization::base_object<ModelBase>(*this);
        ar & _spam;
        ar & _name;
        ar & _int_container;
    }
};

//BOOST_CLASS_VERSION(SpamModel, 1)

void inline save_spam_text_iarchive(const SpamModel &s, const char * filename){
    // make an archive
    std::ofstream ofs(filename);
    boost::archive::text_oarchive oa(ofs);
    oa << s;
}

void inline load_spam_text_oarchive(SpamModel &s, const char * filename)
{
    // open the archive
    std::ifstream ifs(filename);
    boost::archive::text_iarchive ia(ifs);
    // restore the schedule from the archive
    ia >> s;
}

void inline save_spam_binary_iarchive(const SpamModel &s, const char * filename){
    // make an archive
    std::ofstream ofs(filename);
    boost::archive::binary_oarchive oa(ofs);
    oa << s;
}

void inline load_spam_binary_oarchive(SpamModel &s, const char * filename)
{
    // open the archive
    std::ifstream ifs(filename);
    boost::archive::binary_iarchive ia(ifs);
    // restore the schedule from the archive
    ia >> s;
}

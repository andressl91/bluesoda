#pragma once

#include <string>
#include <map>
#include "bluesoda/model/model.h"
#include "bluesoda/service/lru_cache.h"

template <typename T>
struct ModelRepository {
public:
    ModelRepository() : _lru_cache{10} {
        // Add parameter to set cache limit of _lru_cache.
        
    }
    ModelRepository(size_t cap) : _lru_cache{cap} {} 
    T read_model(int model_id, bool cache=true, bool refresh_cache=true) {
        if (_lru_cache.contains(model_id)) {
            return _lru_cache.get(model_id);
    }
    return T();
    }
    
    void store_model(T model) {
          _lru_cache.add(model.get_model_id(), model);
    }
    
    auto get_capacity() {return _lru_cache.capacity(); }
    

private:
    lru_cache<int, T, std::map> _lru_cache;
};

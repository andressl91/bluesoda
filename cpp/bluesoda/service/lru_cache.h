#pragma once


#include <stdexcept>
#include <cassert> 
#include <list> 
 
// LRU ache found https://timday.bitbucket.io/lru.html#x1-8007r1
// Start simple, several other cache types on site, look into later.
 
// Class providing fixed-size (by number of records) 
// LRU-replacement cache of a function with signature 
// V f(K). 
// MAP should be one of std::map or std::unordered_map. 
// Variadic template args used to deal with the 
// different type argument signatures of those 
// containers; the default comparator/hash/allocator 
// will be used. 
template < 
  typename K, 
  typename V, 
  template<typename...> class MAP > 

class lru_cache 
{ 
public: 
 
  using key_type = K; 
  using value_type = V;
 
  // Key access history, most recent at back 
  using key_tracker_type = std::list<key_type> ; 
 
  // Key to value and key history iterator 
  using key_to_value_type = MAP<key_type, 
               std::pair<value_type, typename key_tracker_type::iterator> 
               > ; 
 
  // Constuctor specifies the cached function and 
  // the maximum number of records to be stored 
    lru_cache(size_t c) 
    : _capacity(c) 
  { 
    assert(_capacity!=0); 
  } 
    
    auto capacity() {
        return _capacity;
    }
    const auto elements() const{
    return _key_to_value.size();
    }
    
    
    
    bool contains(const key_type& k) {
        const typename key_to_value_type::iterator it =_key_to_value.find(k); 
        if (it==_key_to_value.end()) { 
            return false;
        }
        return true;
    }
    
  // Obtain value of the cached function for k 
  //ORIG  value_type operator()(const key_type& k) { 
    void add(const key_type& k, const value_type& v) { 
 
    // Attempt to find existing record 
    const typename key_to_value_type::iterator it =_key_to_value.find(k); 
    if (it==_key_to_value.end()) { 
 
      // We don't have it: 
      // Create new record and return value
        insert(k, v); 
 
    } 
    else {  
      // Return the retrieved value
       //Update key with new value
      (*it).second.first = v; 
    
    } 
  }
  
  value_type get(const key_type& k) {
      
    // Attempt to find existing record 
    const typename key_to_value_type::iterator it =_key_to_value.find(k); 
    if (it==_key_to_value.end()) {
        throw std::runtime_error(std::string("key not found in LRU cache."));
    }
    // Update access record by moving 
    // accessed key to back of list 
      _key_tracker.splice( 
       _key_tracker.end(), 
       _key_tracker, 
        (*it).second.second 
      ); 
 
      // Return the retrieved value 
      return (*it).second.first; 
  }
  //Remove element if exist in cache
  void remove(const key_type& k) {
    const typename key_to_value_type::iterator it =_key_to_value.find(k); 
        if(it != _key_to_value.end()) {
            // Erase both elements to completely purge record 
            _key_tracker.erase(it->second.second); 
            _key_to_value.erase(it); 
            
      }
  }
 
  // Obtain the cached keys, most recently used element 
  // at head, least recently used at tail. 
  // This method is provided purely to support testing. 
  template <typename IT> void get_keys(IT dst) const { 
    typename key_tracker_type::const_reverse_iterator src 
        =_key_tracker.rbegin(); 
    while (src!=_key_tracker.rend()) { 
      *dst++ = *src++; 
    } 
  } 
 
private: 
 
  // Method is only called on cache misses 
  // Record a fresh key-value pair in the cache 
  void insert(const key_type& k,const value_type& v) { 
 
    
    assert(_key_to_value.find(k)==_key_to_value.end()); 
 
    // Make space if necessary 
    if (_key_to_value.size()==_capacity) 
      evict(); 
 
    // Record k as most-recently-used key 
    typename key_tracker_type::iterator it 
      =_key_tracker.insert(_key_tracker.end(),k); 
 
    // Create the key-value entry, 
    // linked to the usage record. 
    _key_to_value.insert( 
      std::make_pair( 
        k, 
        std::make_pair(v,it) 
      ) 
    ); 
    // No need to check return, 
    // given previous assert. 
  } 
 
  // Purge the least-recently-used element in the cache 
  void evict() { 
 
    // Assert method is never called when cache is empty 
    assert(!_key_tracker.empty()); 
 
    // Identify least recently used key 
    const typename key_to_value_type::iterator it 
      =_key_to_value.find(_key_tracker.front()); 
    assert(it!=_key_to_value.end()); 
 
    // Erase both elements to completely purge record 
    _key_to_value.erase(it); 
    _key_tracker.pop_front(); 
  } 
 
  // The function to be cached 
  value_type (*_fn)(const key_type&); 
 
  // Maximum number of key-value pairs to be retained 
  const size_t _capacity; 
 
  // Key access history 
  key_tracker_type _key_tracker; 
 
  // Key-to-value lookup 
  key_to_value_type _key_to_value; 
}; 
 

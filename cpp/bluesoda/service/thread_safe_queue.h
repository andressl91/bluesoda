#pragma once

#include <queue>
#include <mutex>
#include <utility>

namespace bluesoda {
    
    namespace net {
        
        /**
         * @brief Threadsafe double-ended queue, using mutex.
         * 
         * @details The complexity (efficiency) of common operations on deques is as follows:
         * 
         *  * Random access - constant O(1)
            * Insertion or removal of elements at the end or beginning - constant O(1)
            * Insertion or removal of elements - linear O(n) 
         */
        
        
        template<typename T>
        class mqueue {
        public:
            mqueue() = default;
            
            // Avoid copying queue, along with mutex.
            mqueue(const mqueue<T>&) = delete;
            
            // Remove all elements in queue, @see 
            /**
             * @see clear() 
             */
            ~mqueue() { clear(); }
            
            /**
             * @brief Return the number of items contained in the queue.
             */
            size_t count() {
                std::scoped_lock lock(m_mutex);
                return _queue.size();
            }
            
            /**
             * @brief Remove all elements in queue.
             */
            void clear() {
                std::scoped_lock lock(m_mutex);
                _queue.clear();
            }
            
            /**
             * @brief Check if queue is empty.
             */
            bool empty() {
                std::scoped_lock lock(m_mutex);
                return _queue.empty();
            }
            
            const T& front() {
                std::scoped_lock lock(m_mutex);
                return _queue.front();
            }
            
            void emplace_front(const T& obj) {
                std::scoped_lock lock(m_mutex);
                _queue.emplace_front(obj);
            }
            
            const T& back() {
                std::scoped_lock lock(m_mutex);
                return _queue.back();
            }
            
            void emplace_back(const T& obj) {
                std::scoped_lock lock(m_mutex);
                _queue.emplace_back(obj);
                }
            /**
             * @brief Remove and return first item in queue
             */
            T pop_front() {
                std::scoped_lock lock(m_mutex);
                auto first_item = std::move(_queue.front());
                _queue.pop_front();
                return first_item;
            }
            
             /**
             * @brief Remove and return last item in queue
             */
            T pop_back() {
                std::scoped_lock lock(m_mutex);
                auto last_item = std::move(_queue.back());
                _queue.pop_back();
                return last_item;
            }
            
            
        protected:
            std::deque<T> _queue;
            std::mutex m_mutex;
            
            
        };
        
    }
}

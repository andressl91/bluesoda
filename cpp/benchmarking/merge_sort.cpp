//#include <Criterion-1.0/criterion/criterion.hpp>
#include <criterion/criterion.hpp>
template<typename RandomAccessIterator, typename Compare>
void merge_sort(RandomAccessIterator first, RandomAccessIterator last,
                Compare compare, std::size_t size) {
    if (size < 2) return;
    auto middle = first + size / 2;
    merge_sort(first, middle, compare, size / 2);
    merge_sort(middle, last, compare, size - size/2);
    std::inplace_merge(first, middle, last, compare);
}


BENCHMARK(MergeSort, std::size_t) // <- one parameter to be passed to the benchmark
{
    SETUP_BENCHMARK(
            const auto size = GET_ARGUMENT(0); // <- get the argument passed to the benchmark
            std::vector<int> vec(size, 0);
    )

    // Code to be benchmarked
    merge_sort(vec.begin(), vec.end(), std::less<int>(), size);

    TEARDOWN_BENCHMARK(
            vec.clear();
    )
}

// Run the above benchmark for a number of inputs:

INVOKE_BENCHMARK_FOR_EACH(MergeSort,
                          ("/10", 10),
                          ("/100", 100),
                          ("/1K", 1000),
                          ("/10K", 10000),
                          ("/100K", 100000)
)

CRITERION_BENCHMARK_MAIN()
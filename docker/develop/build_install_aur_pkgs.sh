#!/bin/bash
export CMAKE_BUILD_PARALLEL_LEVEL=4
tmpdir=$(mktemp -d)
aur_pkgs="doctest"
# for some reason this one fails on wget: python-bokeh
#aur_pkgs="doctest dlib"
any_failed=0
pacman --noconfirm -Sy
pushd ${tmpdir}
for pkg in ${aur_pkgs}; do
  echo "build ${pkg}"
  wget -qO - https://aur.archlinux.org/cgit/aur.git/snapshot/${pkg}.tar.gz | tar -xvz
  pushd ${pkg}
  makepkg -cf
  sudo pacman --noconfirm -U ${pkg}-*.pkg*
  popd
done

git clone https://github.com/p-ranav/criterion
pushd criterion 
#mkdir build && cd build
#cmake .. -DCMAKE_INSTALL_PREFIX="${PROJECT_DEPENDENCIES}" ${cmake_common} -DCMAKE_CXX_STANDARD=17
#make install
bash generate_single_include.bash
sudo cp -rf single_include/* /usr/include
popd

#sudo pip install pint 
sudo rm -rf ${tmpdir}
